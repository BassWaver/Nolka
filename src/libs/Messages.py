"""
Messages module for a Discord bot named Nolka.

Author : Zero <dakoolstwunn@gmail.com>
DOCS : Coming to readthedocs.io soon
"""

# Error messages
missingArgs = "I'm missing some arguments"
badArgs = "Those arguments don't work for me"
badChannel = "The channel {} doesn't exist on this server"
noAdmin = "I don't see you on the list"
noSubcommand = "I'm missing a subcommand"

# Color messages
colorSet = "Set the {} color to {}"
colorTypes = "These are the valid color types\n{}"
badColor = "The color {} isn't a good hexadecimal color"

# Role messages
rolesMade = "Created the roles {}"
rolesGiven = "Gave the users {} the roles {}"
rolesTaken = "Took from the users {} the roles {}"
rolesKilled = "Killed the roles {}"
rolesRemoved = "Removed the roles {}"
noRoles = "I can't find any roles"
noMembers = "I can't find any users"

#Invite messages
inviteMessage = "Add me to your server [here]({})"

#Booru messages
descSingleImage = "Rating: {}\n[Source]({})"
descNoSource = "Rating: {}\nNo source provided"

#Booru Error message
noPosts = "No images found with the tags {}"
dumpIndex = "Image {} of {}"
largeDump = "{} is too many images for me to dump at once"
